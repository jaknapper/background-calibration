import glob
import time
import cv2
import numpy as np
import matplotlib.pyplot as plt
#from scipy import stats

# folder containing all images
path = "C:\\Users\\jakna\\OneDrive - University of Bath\\Uni\\OFM\\Pi_images\\micrographs\\SCAN_2019-09-05_21-19-03\\*.jpeg"
#print(path)

number_of_images = len(glob.glob(path))

input_images = []
name = 0

for fname in glob.glob(path):
    new_image = cv2.imread(fname)
    input_images.append(new_image)
 
input_images = np.asarray(input_images)

bin = np.arange(255, step=30)
binned = np.linspace(0,256,128)

B = (input_images[:,400,1,0]).flatten()
G = (input_images[:,400,1,1]).flatten()
R = (input_images[:,400,1,2]).flatten()

# plt.hist([B,G,R],label=['B','G','R'],color=['blue','green','red'])

plt.hist(B,label=['B'],color=['blue'],alpha = 0.4,bins=binned)
plt.hist(G,label=['G'],color=['green'],alpha=0.4,bins=binned)
plt.hist(R,label=['R'],color=['red'],alpha=0.4,bins=binned)

plt.xticks(bin-.5, bin)
plt.xlabel('Colour')
plt.ylabel('Frequency')
#plt.yscale('log')
plt.title('Colour map')
plt.legend(prop={'size': 10})

plt.show()