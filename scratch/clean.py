import glob
import time
import cv2
import numpy as np
import matplotlib.pyplot as plt
import os

from scipy import stats

data_dir = os.path.join("..","SCAN_2020-02-19_18-23-02")

# folder containing all images
glob_path = os.path.join(data_dir,"*.jpeg")

input_images = []

for fname in glob.glob(glob_path):
    new_image = cv2.imread(fname)
    input_images.append(new_image)

input_images = np.asarray(input_images)

sorted_ims = np.sort(input_images,axis=0)
output = np.mean(sorted_ims[6:-2,:,:,:],axis=0).astype(np.uint8)

#output = np.median(input_images, axis = 0).astype(np.uint8)

#output,count = stats.mode(input_images, axis = 0)
#output = output.astype(np.uint8)

#rows, cols, _channels = map(int, output.shape)

#dim = (cols // 5, rows // 5)
#output = cv2.resize(cv2.resize(output, dim, interpolation=cv2.INTER_AREA),(cols,rows), interpolation=cv2.INTER_LANCZOS4)

out_dir = os.path.join(data_dir,"results")

if os.path.exists(out_dir):
    assert os.path.isdir(out_dir), f"{out_dir} is a file!?"
else:
    os.makedirs(out_dir)

out_path = os.path.join(out_dir,"ideal.jpeg")
print(cv2.imwrite(out_path, output))

# cv2.imshow("preview_mode", output)
# cv2.waitKey(0)
# cv2.destroyAllWindows