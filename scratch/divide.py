import cv2
import glob
import numpy as np
import os

def safe_divide(A,B):
    old_err_state = np.seterr(divide='ignore')
    C =A/B
    C[np.isinf(C)] = 255
    np.seterr(**old_err_state)
    return C

# path contains all of the jpeg images to be calibrated
data_dir = os.path.join("..","SCAN_2020-02-19_18-23-02")

# folder containing all images
glob_path = os.path.join(data_dir,"*.jpeg")

# background is the scratch-free calibration images
background = cv2.imread(os.path.join(data_dir,'results','ideal.jpeg'))

images = []

for fname in glob.glob(glob_path):
    new_image=cv2.imread(fname)
    images.append(new_image)

# converts the list of images into a 4D numpy array for easier maths and iterations
images = np.asarray(images)

# dividing all images by the background image gives equalised but very dark images (typical RGB values are around [1,1,1])
dark = safe_divide(images,background)

# find the brightest channel of any pixel (largest R, G or B) from all dark images, and rescale all images to the maximum available range without any saturation
brightest_output = np.amax((np.amax(np.amax(dark, axis = 0), axis = 0)),axis=0)
scale_factor = 255 / np.amax(brightest_output)
result = (dark * scale_factor)
result = result.astype(np.uint8)

print('i got this far')

out_dir = os.path.join(data_dir,"results")

if os.path.exists(out_dir):
    assert os.path.isdir(out_dir), f"{out_dir} is a file!?"
else:
    os.makedirs(out_dir)

for i in range(len(result)):
    out_path = os.path.join(out_dir,f"out_{i:02d}.jpeg")
    print(cv2.imwrite(out_path, result[i]))
    
    # cv2.imshow("before", images[i])
    # cv2.imshow("middle", background)
    # cv2.imshow("after", result[i])
    # cv2.waitKey(0)
    # cv2.destroyAllWindows()